import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, NavbarBrand } from "reactstrap";

function NavBar({
  navText,
  firstButton,
  secondButton,
  refButton1,
  refButton2,
  username,
  funcButton2,
  activeHome,
  activeGame,
  activeUser,
}) {
  return (
    <nav
      class="navbar bg-transparent border-bottom border-body navbar-expand-lg bg-body-tertiary"
      data-bs-theme="dark"
    >
      <div class="container">
        <NavbarBrand href="/">
          <img
            className="me-3"
            alt="logo"
            src={`assets/image/logo.png`}
            style={{
              height: 40,
              width: 40,
            }}
          />
          {navText}
          {username}
        </NavbarBrand>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item mx-3">
              <a className={`nav-link ${activeHome}`} href="/homepage">
                Home
              </a>
            </li>
            <li className="nav-item me-3">
              <a className={`nav-link ${activeGame}`} href="/game-list">
                Game List
              </a>
            </li>
            <li className="nav-item">
              <a className={`nav-link ${activeUser}`} href="/user-list">
                User List
              </a>
            </li>
          </ul>
        </div>
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="collapse navbar-collapse justify-content-end"
          id="navbarSupportedContent"
        >
          <form class="d-flex" role="button">
            <div>
              <Button color="light" outline href={refButton1} className="me-2">
                {firstButton}
              </Button>
              <Button
                color="light"
                outline
                href={refButton2}
                onClick={funcButton2}
              >
                {secondButton}
              </Button>
            </div>
          </form>
        </div>
      </div>
    </nav>
  );
}

export default NavBar;
