import { useState, useEffect } from "react";
import "./DetailGame.css";
import { db } from "../../store/firebase";
import { collection, getDocs } from "firebase/firestore";
import NavBar from "../../components/NavBar";
import { useAuth } from "../../store/AuthContext";
import { Container, Row, Col, Table } from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const DetailGame = () => {
  const [users, setUsers] = useState([]);
  const [username, setUsername] = useState("");
  const auth = useAuth();

  const usersSorting = [...users].sort((a, b) => b.total_score - a.total_score);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const usersRef = collection(db, "users");
        const querySnapshot = await getDocs(usersRef);

        const userList = [];
        querySnapshot.forEach((doc) => {
          userList.push(doc.data());
        });

        setUsers(userList);
      } catch (error) {
        console.error("Error fetching users:", error);
      }
    };

    fetchUsers();

    const currentUserUsername = localStorage.getItem("currentUserUsername");
    console.log(currentUserUsername);
    if (currentUserUsername) {
      setUsername(currentUserUsername);
    }
  }, []);

  return (
    <>
      <NavBar
        navText="Welcome "
        username={username}
        firstButton="Profile"
        secondButton="Logout"
        refButton1="/profile"
        refButton2="/"
        funcButton2={auth.logout}
      />
      <div className="container-fluid p-3">
        <div className="row d-flex justify-content-center mt-5">
          <Container className="col-8">
            <h1 className="text-center fw-bold animate__animated animate__fadeInUp mb-5">
              Detail Game
            </h1>
            <Row className="justify-content-center animate__animated animate__fadeInUp">
              <Col xs="12">
                <div class="card w-100 ">
                  <img
                    src={`assets/image/rps.avif`}
                    className="card-img-top "
                    height={225}
                    style={{ objectFit: "cover" }}
                    alt="..."
                  />
                  <div class="card-body row d-flex flex-column">
                    <h5 class="card-title fw-bold">ROCK PAPER SCISSORS</h5>
                    <p class="card-text">
                      Rock, Paper, Scissors adalah permainan sederhana di mana
                      pemain akan melawan computer untuk memilih antara Rock,
                      Paper, atau Scissors <br />
                      <br />
                      Aturan utamanya adalah : <br /> 1. Rock mengalahkan
                      Scissors, Scissors mengalahkan Paper, Paper mengalahkan
                      Rock <br />
                      2. Jika Player menang maka Player akan mendapatkan +3
                      Point sedangkan jika Player kalah maka akan mendapatkan -1
                      Point dan jika Draw maka tidak mendapatkan point
                    </p>
                    <div className="col-12 d-flex justify-content-center mt-3">
                      <a href="/rps" class="btn btn-primary w-25">
                        PLAY
                      </a>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
          <Container className="col-3 mb">
            <h1 className="text-center fw-bold animate__animated animate__fadeInUp mb-5">
              Leaderboard
            </h1>
            <Row className="d-flex justify-content-center animate__animated animate__fadeInUp row">
              <Col xs="12">
                <Table striped>
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>Total Score</th>
                    </tr>
                  </thead>
                  <tbody>
                    {usersSorting.map((user) => {
                      return <User user={user} />;
                    })}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </>
  );
};

function User({ user }) {
  return (
    <tr key={user.id}>
      <td>{user.username}</td>
      <td className="fw-bold">{user.total_score}</td>
    </tr>
  );
}

export default DetailGame;
