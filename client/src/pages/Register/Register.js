import { useState } from "react";
import { FormGroup, Input, Label } from "reactstrap";
import "./Register.css";
import FormContainer from "../../components/Form/FormContainer";
import { useNavigate } from "react-router-dom";
import { auth, db } from "../../store/firebase";
import { collection, addDoc, query, where, getDocs } from "firebase/firestore";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { toast } from "react-toastify";
import NavBar from "../../components/NavBar";

function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const navigate = useNavigate();
  const userRef = collection(db, "users");

  const validatePassword = () => {
    if (password.length < 8) {
      setError("Password should be at least 8 characters long!");
      return false;
    }
    return true;
  };

  const validateEmail = async () => {
    const emailQuery = query(userRef, where("email", "==", email));
    const emailQuerySnapshot = await getDocs(emailQuery);
    if (!emailQuerySnapshot.empty) {
      setError("Email is already in use!");
      return false;
    }
    return true;
  };

  const validateUsername = async () => {
    const usernameQuery = query(userRef, where("username", "==", username));
    const usernameQuerySnapshot = await getDocs(usernameQuery);
    if (!usernameQuerySnapshot.empty) {
      setError("Username is already in use!");
      return false;
    }
    return true;
  };

  const handleNewUserRegistration = async (e) => {
    e.preventDefault();

    try {
      if (!validatePassword()) return;
      if (!(await validateEmail())) return;
      if (!(await validateUsername())) return;

      const user = await createUserWithEmailAndPassword(auth, email, password);

      console.log(user);

      try {
        const userData = {
          username: username,
          email: email,
          bio: "",
          city: "",
          total_score: 0,
          social_media: "",
          user_uid: user.user.uid,
        };
        await addDoc(userRef, userData);
      } catch (error) {
        console.log(error.message);
      }

      navigate("/login");
    } catch (e) {
      console.log(e.message);
    }
  };

  return (
    <>
      <NavBar
        navText="Welcome to Our Game"
        firstButton="Login"
        secondButton="Register"
        refButton1="/login"
        refButton2="/register"
      />
      <div className="container-form container-fluid d-flex justify-content-center align-content-center row animate__animated animate__fadeInUp">
        <FormContainer
          formText="Register"
          buttonText="Register"
          onClickFunction={handleNewUserRegistration}
        >
          {error && <div className="error-message text-danger">{error}</div>}
          <FormGroup>
            <Label for="username">Username</Label>
            <Input
              id="username"
              name="username"
              placeholder="Input username"
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="email">Email</Label>
            <Input
              id="email"
              name="email"
              placeholder="Input email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input
              id="password"
              name="password"
              placeholder="Input password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormGroup>
        </FormContainer>
      </div>
    </>
  );
}

export default Register;
