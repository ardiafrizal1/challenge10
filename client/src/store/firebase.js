import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyBirPk2m1RPKiGQJHhaZxX7EqGnrS1rMB4',
  authDomain: 'chapter-9-4173e.firebaseapp.com',
  projectId: 'chapter-9-4173e',
  storageBucket: 'chapter-9-4173e.appspot.com',
  messagingSenderId: '70719836302',
  appId: '1:70719836302:web:eabb05233c14fd6eb2b9df',
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app);

export { auth, db, storage };
