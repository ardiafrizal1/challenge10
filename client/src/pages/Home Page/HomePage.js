import "./HomePage.css";
import { Button } from "reactstrap";
import NavBar from "../../components/NavBar";
import { useAuth } from "../../store/AuthContext";
import { useEffect, useState } from "react";

function HomePage() {
  const [username, setUsername] = useState();

  const auth = useAuth();

  useEffect(() => {
    const currentUserUsername = localStorage.getItem("currentUserUsername");
    console.log(currentUserUsername);
    if (currentUserUsername) {
      setUsername(currentUserUsername);
    }
  }, []);

  return (
    <>
      <NavBar
        navText="Welcome back, "
        username={username}
        firstButton="Profile"
        secondButton="Logout"
        refButton1="/profile"
        refButton2="/"
        funcButton2={auth.logout}
        activeHome="active"
      />
      <div className="container-hero d-flex justify-content-center my-5 row w-100 animate__animated animate__fadeInUp">
        <div className="col-10 border border-light shadow-sm rounded bg-white">
          <div className="row d-flex flex-column align-content-center align-items-center p-4">
            <div className="col-12">
              <img
                src="assets/image/game.jpg"
                style={{ objectFit: "cover" }}
                alt="rps.jpg"
                className="image w-100 rounded mb-3"
              />
              <h2>Welcome to Gaming Website!</h2>
              <p>
                Welcome to GameHub, your one-stop online gaming destination!
                Enjoy a variety of games for all ages and interests. Whether
                you're into strategy, action, or puzzles, we've got you covered.
                Compete with friends or go solo in a user-friendly, social
                gaming experience. Join the fun at GameHub and let the games
                begin!
              </p>
              <p>
                With a user-friendly interface and seamless navigation, our
                website makes it easy to discover and enjoy your favorite games.
                From classic card games like Solitaire to action-packed
                adventures that will keep you on the edge of your seat, our
                library is constantly expanding to ensure there's always
                something new to try.
              </p>
              <p>
                So, what are you waiting for? Join the fun at GameHub and let
                the games begin! Whether you have a few minutes to spare or want
                to embark on an epic gaming marathon, we've got you covered.
                Play, explore, and conquer – all in one convenient place.
              </p>
            </div>
            <div className="row d-flex justify-content-center">
              <div className="col-6 d-flex justify-content-center">
                <Button
                  className="mt-4 w-50 "
                  color="primary"
                  href="/game-list"
                >
                  Play Game
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default HomePage;
