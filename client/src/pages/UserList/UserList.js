// UserList.js
import { useState, useEffect } from "react";
import { db } from "../../store/firebase";
import { collection, getDocs } from "firebase/firestore";
import NavBar from "../../components/NavBar";
import { Table } from "reactstrap";
import { useAuth } from "../../store/AuthContext";

function UserList() {
  const [users, setUsers] = useState([]);
  const [username, setUsername] = useState();
  const auth = useAuth();

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const usersRef = collection(db, "users");
        const querySnapshot = await getDocs(usersRef);

        const userList = [];
        querySnapshot.forEach((doc) => {
          userList.push(doc.data());
        });

        setUsers(userList);
      } catch (error) {
        console.error("Error fetching users:", error);
      }
    };

    fetchUsers();

    const currentUserUsername = localStorage.getItem("currentUserUsername");
    if (currentUserUsername) {
      setUsername(currentUserUsername);
    } else if (auth.currentUserObj) {
      setUsername(auth.currentUserObj.username);
    }
  }, [auth.currentUserObj]);

  return (
    <>
      <NavBar
        navText="Welcome "
        username={username}
        firstButton="Profile"
        secondButton="Logout"
        refButton1="/profile"
        refButton2="/"
        funcButton2={auth.logout}
        activeUser="active"
      />
      <div>
        <div className="p-3 ">
          <h1>User List</h1>
          <TableUsers users={users} />
        </div>
      </div>
    </>
  );
}

function TableUsers({ users }) {
  return (
    <>
      <Table striped>
        <thead>
          <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Bio</th>
            <th>City</th>
            <th>Social Media</th>
            <th>Total Score</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => {
            return <User user={user} />;
          })}
        </tbody>
      </Table>
    </>
  );
}

function User({ user }) {
  return (
    <tr key={user.id}>
      <td>{user.username}</td>
      <td>{user.email}</td>
      <td>{user.bio}</td>
      <td>{user.city}</td>
      <td>{user.social_media}</td>
      <td>{user.total_score}</td>
    </tr>
  );
}

export default UserList;
