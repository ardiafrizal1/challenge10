import './LandingPage.css';
import NavBar from '../../components/NavBar';

function LandingPage() {
  return (
    <>
      <NavBar navText="Welcome to Our Game" firstButton="Login" secondButton="Register" refButton1="/login" refButton2="/register" />
      <div className="hero d-flex align-items-center">
        <div className="container">
          <div className="row">
            <div className="col text-center">
              <h1 className="text-white animate__animated animate__fadeInUp">
                Welcome to Kelompok 1 <br />
                Fullstack Web Development 34
              </h1>
              <p className="text-white animate__animated animate__fadeInUp animate__delay-1s">Project ini dibuat dengan bangga oleh kami</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default LandingPage;
