import { createContext, useContext, useEffect } from 'react';
import { auth, db } from '../store/firebase';
import { collection, query, getDocs, where } from 'firebase/firestore';

const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  // const [currentUser, setCurrentUser] = useState();
  // const [currentUserObj, setCurrentUserObj] = useState();

  const logout = async () => {
    console.log('logout called');
    try {
      await auth.signOut();
      // setCurrentUser(null);
      // setCurrentUserObj(null);
      localStorage.setItem('currentUser', null);
      localStorage.setItem('currentUserUid', null);
      localStorage.setItem('currentUserUsername', null);
      localStorage.setItem('currentUserCollectionId', null);
    } catch (error) {
      console.error('Error logging out:', error);
    }
  };

  const getUser = async (authUser) => {
    const userId = authUser.uid;
    let userData;

    const userRef = collection(db, 'users');
    const userQuery = query(userRef, where('user_uid', '==', userId));
    const nextUserSnapshot = await getDocs(userQuery);

    if (!nextUserSnapshot.empty) {
      nextUserSnapshot.forEach((doc) => {
        userData = doc.data();
      });
    }
    return userData;
  };

  const getUserId = async (authUser) => {
    const userId = authUser.uid;
    let userCollectionId;

    const userRef = collection(db, 'users');
    const userQuery = query(userRef, where('user_uid', '==', userId));
    const nextUserSnapshot = await getDocs(userQuery);

    if (!nextUserSnapshot.empty) {
      nextUserSnapshot.forEach((doc) => {
        userCollectionId = doc.id;
      });
    }
    return userCollectionId;
  };

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const authUser = auth.currentUser;
        if (authUser) {
          // setCurrentUser(authUser);
          localStorage.setItem('currentUser', authUser);
          const userData = await getUser(authUser);
          localStorage.setItem('currentUserUid', userData.user_uid);
          localStorage.setItem('currentUserUsername', userData.username);
          const userCollectionId = await getUserId(authUser);
          localStorage.setItem('currentUserCollectionId', userCollectionId);
          // setCurrentUserObj(userData || {});
        }
        // } else {
        //   setCurrentUser(null);
        //   setCurrentUserObj(null);
        // }
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    const unsubscribe = auth.onAuthStateChanged((authUser) => {
      fetchUserData();
    });

    return () => {
      unsubscribe();
    };
  }, []);

  const value = {
    // currentUser,
    // currentUserObj,
    logout,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
