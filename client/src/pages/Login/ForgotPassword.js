import React, { useState } from 'react';
import { auth } from '../../store/firebase';
import { Button } from 'reactstrap';

function ForgotPassword() {
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const handleResetPassword = (e) => {
    e.preventDefault();
    auth
      .sendPasswordResetEmail(email)
      .then(() => {
        setMessage('Email untuk mereset kata sandi telah dikirim.');
      })
      .catch((error) => {
        setMessage(`Error: ${error.message}`);
      });
  };

  return (
    <div>
      <h2>Lupa Kata Sandi</h2>
      <input type="email" placeholder="Alamat Email" value={email} onChange={(e) => setEmail(e.target.value)} />
      <br></br>
      <br></br>
      <Button onClick={handleResetPassword}>Reset Kata Sandi</Button>
      <p>{message}</p>
    </div>
  );
}

export default ForgotPassword;
