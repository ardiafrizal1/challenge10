import { useState, useEffect } from 'react';
import './Rps.css';
import NavBar from '../../components/NavBar';
import { db } from '../../store/firebase';
import { doc, updateDoc, collection, where, getDocs, query } from 'firebase/firestore';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../store/AuthContext';

const choices = ['rock', 'paper', 'scissor'];

function Rps() {
  const [playerChoice, setPlayerChoice] = useState(null);
  const [computerChoice, setComputerChoice] = useState(null);
  const [result, setResult] = useState(null);
  const [total_score, setScore] = useState(null);
  const [username, setUsername] = useState();

  const auth = useAuth();

  useEffect(() => {
    const currentUserUsername = localStorage.getItem('currentUserUsername');
    console.log(currentUserUsername);
    if (currentUserUsername) {
      setUsername(currentUserUsername);
    }
    // console.log(auth.currentUserObj)
    // if(auth.currentUserObj){
    //   setUsername(auth.currentUserObj.username)
    //   console.log(auth.currentUserObj.user_uid)
    // }
  }, []);

  const currentUserUid = localStorage.getItem('currentUserUid');
  const currentUserCollectionId = localStorage.getItem('currentUserCollectionId');

  const navigate = useNavigate();

  const randomChoice = () => choices[Math.floor(Math.random() * choices.length)];

  //Gameplay rules & Scoring method,
  // Increment the total_score when the player wins & decrement when lose
  const playGame = (choice) => {
    const computer = randomChoice();
    setPlayerChoice(choice);
    setComputerChoice(computer);
  
    if (choice === computer) setResult('draw');
    else if ((choice === 'rock' && computer === 'scissor') || (choice === 'paper' && computer === 'rock') || (choice === 'scissor' && computer === 'paper')) {
      setResult('win');
      const newScore = total_score + 3;
      setScore(newScore);
      scoreUpdate(newScore);
    } else {
      setResult('lose');
      setScore(total_score - 1);
      scoreUpdate(total_score - 1);
    }
  };
  

  // Update score method for the total_score of current user
  const scoreUpdate = async (newScore) => {
    try {
      console.log('Updating total_score for user ID:', currentUserCollectionId);
      console.log('New total_score:', newScore);

      await updateDoc(doc(db, 'users', currentUserCollectionId), {
        total_score: newScore,
      });

      console.log('Score updated successfully');
    } catch (error) {
      console.error('Error updating Score:', error);
    }
  };
  

  useEffect(() => {
    if (currentUserUid === null) {
      console.log('navigate called');
      navigate('/login');
    } else {
      console.log('fetch called');
      const fetchUserScore = async () => {
        try {
          const userRef = collection(db, 'users');
          const userQuery = query(userRef, where('user_uid', '==', currentUserUid));
          const nextUserSnapshot = await getDocs(userQuery);

          let userData;

          if (!nextUserSnapshot.empty) {
            nextUserSnapshot.forEach((doc) => {
              userData = doc.data();
            });
          }
          if (userData) {
            setScore(userData.total_score || 0);
          }
        } catch (error) {
          console.error('Error fetching user score:', error);
        }
      };

      fetchUserScore();
    }
  }, []);

  return (
    <>
      <NavBar navText="Welcome back, " username={username} firstButton="Profile" secondButton="Logout" refButton1="/profile" refButton2="/" funcButton2={auth.logout} />
      <section className="body">
        <div className="container-fluid">
          <div className="d-flex justify-content-around ">
            <h1>
              {' '}
              <span>Rock Paper Scissors</span>
            </h1>
          </div>
          <div>
            <div className="d-flex justify-content-around ">
              <h2>Score : {total_score}</h2>
            </div>
          </div>
          <div className="d-flex justify-content-around">
          <PlayerChoice choices={choices} playGameFunc={playGame} scoreUpdateFunc={scoreUpdate} />
            <Result playerChoice={playerChoice} computerChoice={computerChoice} result={result} />
            <ComputerChoice choices={choices} computerChoice={computerChoice} />
          </div>
        </div>
      </section>
    </>
  );


  function PlayerChoice({ choices, playGameFunc, scoreUpdateFunc }) {
    return (
      <form onSubmit={scoreUpdateFunc}>
        <div className="col-4 d-flex flex-column align-items-center player-group">
          <h2>Player</h2>
          <div className="player-button-group">
            {choices.map((choice) => (
              <div className={`${choice}-button-group`} key={choice}>
                <button type="submit" className={`btn ${choice}-p`} key={choice} value={total_score} onClick={() => playGameFunc(choice)} onChange={(e) => setScore(e.target.value)}>
                  <img src={`assets/image/${choice}.png`} alt={`${choice}.png`} className={`${choice}-image`} />
                </button>
              </div>
            ))}
          </div>
        </div>
      </form>
    );
  }
}
function ComputerChoice({ choices, computerChoice }) {
  return (
    <div class="col-4 d-flex flex-column align-items-center computer-group">
      <h2>COMPUTER</h2>
      <div className="computer-button-group">
        {choices.map((choice) => (
          <div className={`${choice}-button-group`} key={choice}>
            <button className={`btn ${choice}-p`} key={choice} style={computerChoice === choice ? { opacity: '100%' } : {}} disabled>
              <img src={`assets/image/${choice}.png`} alt={`${choice}.png`} className={`${choice}-image`} />
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}

function Result({ playerChoice, computerChoice, result }) {
  return (
    <div className="player-choose col-3 d-flex flex-wrap flex-column align-content-center justify-content-around">
      <div className="container-result">
        <div className={`d-flex container-result-${result} justify-content-center align-items-center flex-column p-3`}>
          {playerChoice && <p className="choose-player">Your choice: {playerChoice}</p>}
          {computerChoice && <p>Computer's choice: {computerChoice}</p>}
          {result && <p>Result: You {result}!</p>}
        </div>
      </div>
    </div>
  );
}

export default Rps;