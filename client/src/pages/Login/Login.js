import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { FormGroup, Input, Label } from 'reactstrap';
import './Login.css';
import FormContainer from '../../components/Form/FormContainer';
import NavBar from '../../components/NavBar';
import { Link } from 'react-router-dom';

// Auth Context

// firebase
import { signInWithEmailAndPassword } from 'firebase/auth';
import { collection, query, where, getDocs } from 'firebase/firestore';
import { auth, db } from '../../store/firebase';

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();
  const [error, setError] = useState(null);

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const userCredential = await signInWithEmailAndPassword(auth, email, password);

      const userEmail = userCredential.user.email;

      const blogRef = collection(db, 'users');
      const nextBlogsQuery = query(blogRef, where('email', '==', userEmail));
      const nextBlogsSnapshot = await getDocs(nextBlogsQuery);

      if (!nextBlogsSnapshot.empty) {
        // Loop through the documents in the snapshot (there should be only one)
        nextBlogsSnapshot.forEach((doc) => {
          const userData = doc.data();
          const username = userData.username;
          console.log(username);
        });
      }
      setError(null);
      navigate('/homepage');
    } catch (e) {
      setError('Email or Password is incorrect!');
    }
  };

  return (
    <>
      <NavBar navText="Welcome to Our Game" firstButton="Login" secondButton="Register" refButton1="/login" refButton2="/register" />
      <div>
        <div className="container-form container-fluid d-flex justify-content-center align-content-center row animate__animated animate__fadeInUp">
          <FormContainer formText="Login" buttonText="Login" onSubmitFunction={handleLogin}>
            {error && <p className="error-message text-danger">{error}</p>}
            <FormGroup>
              <Label for="email">Email</Label>
              <Input id="email" name="email" placeholder="Input email" type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input id="password" name="password" placeholder="Input password" type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </FormGroup>
            <FormGroup>
              <Link to="/reset">Forgot Password?</Link>
            </FormGroup>
          </FormContainer>
        </div>
      </div>
    </>
  );
}

export default Login;
