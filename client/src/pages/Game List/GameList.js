import "./GameList.css";
import React, { useState, useEffect } from "react";
import { Button } from "reactstrap";
import NavBar from "../../components/NavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import { useAuth } from "../../store/AuthContext";

function GameList() {
  const [username, setUsername] = useState();
  const auth = useAuth();

  useEffect(() => {
    const currentUserUsername = localStorage.getItem("currentUserUsername");
    console.log(currentUserUsername);
    if (currentUserUsername) {
      setUsername(currentUserUsername);
    }
    // console.log(auth.currentUserObj)
    // if(auth.currentUserObj){
    //   setUsername(auth.currentUserObj.username)
    //   console.log(auth.currentUserObj.user_uid)
    // }
  }, []);

  return (
    <>
      <NavBar
        navText="Welcome back, "
        username={username}
        firstButton="Profile"
        secondButton="Logout"
        refButton1="/profile"
        refButton2="/"
        funcButton2={auth.logout}
        activeGame="active"
      />
      <div classNameName="container-fluid">
        <div className="row row-cols-1 row-cols-md-3 g-4 mt-3 w-100">
          <div className="col animate__animated animate__fadeInUp animate__delay-1s">
            <div className="card h-100 text-center shadow-sm border-0">
              <img
                src={`assets/image/roullete.avif`}
                className="card-img-top h-75"
                style={{ objectFit: "cover" }}
                alt="roulette.jpg"
              />
              <div className="card-body">
                <h5 className="card-title">Roullete Game</h5>
                <p className="card-text">
                  Game ini untuk kamu yang ingin mencoba keberuntungan
                </p>
              </div>
              <div className="my-3">
                <Button color="primary" href="/rps" className="me-2">
                  Main
                </Button>
                <Button color="primary" outline href="/detail-game">
                  Detail Game
                </Button>
              </div>
            </div>
          </div>
          <div className="col animate__animated animate__fadeInUp">
            <div className="card h-100 text-center shadow-sm border-0">
              <img
                src={`assets/image/rps.avif`}
                className="card-img-top h-75"
                style={{ objectFit: "cover" }}
                alt="rps.jpg"
              />
              <div className="card-body ">
                <h5 className="card-title">Rock Paper Scissors</h5>
                <p className="card-text">
                  Game adu suit siapa yang paling jago
                </p>
              </div>
              <div className="my-3">
                <Button color="primary" href="/rps" className="me-2">
                  Main
                </Button>
                <Button color="primary" outline href="/detail-game">
                  Detail Game
                </Button>
              </div>
            </div>
          </div>
          <div className="col animate__animated animate__fadeInUp animate__delay-2s">
            <div className="card h-100 text-center shadow-sm border-0">
              <img
                src={`assets/image/vr.avif`}
                className="card-img-top h-75"
                style={{ objectFit: "cover" }}
                alt="vr-game.jpg"
              />
              <div className="card-body">
                <h5 className="card-title">The Sims</h5>
                <p className="card-text">
                  Game yang menjadikan dunianya seakan nyata
                </p>
              </div>
              <div className="my-3">
                <Button color="primary" href="/rps" className="me-2">
                  Main
                </Button>
                <Button color="primary" outline href="/detail-game">
                  Detail Game
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default GameList;
