import { Form, Button } from "reactstrap";

export default function FormContainer({
  children,
  formText,
  buttonText,
  onClickFunction,
  onSubmitFunction,
}) {
  return (
    <Form
      className="col-3 p-3 bg-light border border-light shadow-sm p-3 mb-5 rounded d-flex flex-column justify-content-center"
      onSubmit={onSubmitFunction}
    >
      <h2>{formText}</h2>
      {children}
      <Button
        type="submit"
        color="primary"
        className="mt-3"
        onClick={onClickFunction}
      >
        {buttonText}
      </Button>
    </Form>
  );
}
