import React, { useState, useEffect } from 'react';
import { Button, FormGroup, Input, Label } from 'reactstrap';
import NavBar from '../../components/NavBar';
import { useAuth } from '../../store/AuthContext';
import { db } from '../../store/firebase';
import { useNavigate } from 'react-router-dom';
import { doc, updateDoc, collection, where, getDocs, query } from 'firebase/firestore';

function Profile() {
  const [newUsername, setNewUsername] = useState('');
  const [newBio, setNewBio] = useState('');
  const [newCity, setNewCity] = useState('');
  const [newSocialMedia, setNewSocialMedia] = useState('');
  const auth = useAuth();

  const currentUserUid = localStorage.getItem('currentUserUid');
  const currentUserCollectionId = localStorage.getItem('currentUserCollectionId');
  //const currentUser = localStorage.getItem('currentUser');
  const navigate = useNavigate();

  const handleProfileUpdate = async (e) => {
    e.preventDefault();
    try {
      await updateDoc(doc(db, 'users', currentUserCollectionId), {
        username: newUsername,
        bio: newBio,
        city: newCity,
        social_media: newSocialMedia,
      });
      alert('Profile updated successfully');
    } catch (error) {
      console.error('Error updating profile:', error);
      alert('Error updating profile. Please try again later.');
    }
  };

  useEffect(() => {
    if (currentUserUid === null) {
      console.log('navigate called');
      navigate('/login');
    } else {
      console.log('fetch called');
      const fetchUserProfile = async () => {
        try {
          const userRef = collection(db, 'users');
          const userQuery = query(userRef, where('user_uid', '==', currentUserUid));
          const nextUserSnapshot = await getDocs(userQuery);

          let userData;

          if (!nextUserSnapshot.empty) {
            nextUserSnapshot.forEach((doc) => {
              userData = doc.data();
            });
          }
          if (userData) {
            console.log(userData.username);
            setNewUsername(userData.username || '');
            setNewBio(userData.bio || '');
            setNewCity(userData.city || '');
            setNewSocialMedia(userData.social_media || '');
          }
        } catch (error) {
          console.error('Error fetching user profile:', error);
        }
      };

      fetchUserProfile();
    }
  }, []);

  return (
    <>
      <NavBar navText="Edit Your Profile" firstButton="Home" secondButton="Logout" refButton1="/homepage" refButton2="/" funcButton2={auth.logout} />
      <div className="container mt-4 animate__animated animate__fadeInUp">
        <h2>Edit Your Profile</h2>
        <form onSubmit={handleProfileUpdate}>
          <FormGroup>
            <Label for="newUsername">Username</Label>
            <Input type="text" id="newUsername" value={newUsername} onChange={(e) => setNewUsername(e.target.value)} />
          </FormGroup>
          <FormGroup>
            <Label for="newBio">Bio</Label>
            <Input type="text" id="newBio" value={newBio} onChange={(e) => setNewBio(e.target.value)} />
          </FormGroup>
          <FormGroup>
            <Label for="newCity">City</Label>
            <Input type="text" id="newCity" value={newCity} onChange={(e) => setNewCity(e.target.value)} />
          </FormGroup>
          <FormGroup>
            <Label for="newSocialMedia">Social Media</Label>
            <Input type="text" id="newSocialMedia" value={newSocialMedia} onChange={(e) => setNewSocialMedia(e.target.value)} />
          </FormGroup>
          <Button color="primary" type="submit">
            Save Profile
          </Button>
        </form>
      </div>
    </>
  );
}

export default Profile;
