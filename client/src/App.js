import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Register from './pages/Register/Register';
import Login from './pages/Login/Login';
import HomePage from './pages/Home Page/HomePage';
import DetailGame from './pages/Detail Game/DetailGame';
import GameList from './pages/Game List/GameList';
import LandingPage from './pages/LandingPage/Landingpage';
import UserList from './pages/UserList/UserList';
import Profile from './pages/Profile/Profile';
import Rps from './pages/RPS/Rps';
import './app.css';
import ForgotPassword from './pages/Login/ForgotPassword';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/homepage" element={<HomePage />} />
          <Route path="/detail-game" element={<DetailGame />} />
          <Route path="/game-list" element={<GameList />} />
          <Route path="/user-list" element={<UserList />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/rps" element={<Rps />} />
          <Route path="/reset" element={<ForgotPassword />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
